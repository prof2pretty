var mode = 0, low = 0, high = 1, gamma = 1;

function colour(x) {
  return 'hsl(' + Math.round(240 * x) + ',100%,' + Math.round(50 + 25 * x) + '%)';
}

function colourize() {
  var prefix = [ 'top-', '' ];
  for (p in prefix) {
    for (id in profile) {
      var e = document.getElementById(prefix[p] + id);
      if (e) {
        var s = e.style;
        var v = profile[id][mode];
        if (low < v && v <= high) {
          s.display = 'inline-block';
          s.padding = '2px';
          s.backgroundColor = colour(Math.pow(1 - (profile[id][mode] - low) / (high - low), gamma));
        } else {
          s.display = '';
          s.padding = '';
          s.backgroundColor = '';
        }
      }
    }
  }
}

function go() {
  var options = new Object();
  var q = '' + this.location;
  q = q.substring(q.indexOf('?') + 1);
  if (q.indexOf('#') > -1) {
    q = q.substring(0, q.indexOf('#'));
  }
  if (q.length >= 1) {
    var kv = new Object();
    var n = 1;
    while (q.indexOf('&') > -1) {
      kv[n] = q.substring(0, q.indexOf('&'));
      q = q.substring(q.indexOf('&') + 1 );
      n = n + 1;
    }
    kv[n] = q;
    for (var i = 0; i <= n; i = i + 1) {
      var k = (''+kv[i]).substring(0, (''+kv[i]).indexOf('='));
      var v = (''+kv[i]).substring((''+kv[i]).indexOf('=') + 1);
      options[k] = v;
    }
  }
  var def = function(k,v) {
    if (null == options[k]) { return v; } else { return options[k]; }
  };
  var modes     = [ 'ticks',  'rticks',  'bytes',   'rbytes'    ];
  var modeNames = { 'ticks': 0, 'rticks': 1, 'bytes': 2 , 'rbytes': 3 };
  mode  = modeNames[def('mode', 'ticks')]; if (mode == null) { mode = 0; };
  low   = Math.min(Math.max(parseFloat(def('low',   0)), 0),  1);
  high  = Math.min(Math.max(parseFloat(def('high',  1)), 0),  1);
  gamma = Math.min(Math.max(parseFloat(def('gamma', 1)), 0.1), 10);
  document.getElementById('ui-' + modes[mode]).setAttributeNS(null, 'checked', 'checked');
  $('#ui-mode').buttonset();
  $('#ui-ticks').click(function() { mode = modeNames['ticks']; colourize(); });
  $('#ui-bytes').click(function() { mode = modeNames['bytes']; colourize(); });
  $('#ui-limit').slider({ orientation: 'vertical', min: 0, max: 1000, step: 1, range: true,
    values: [1000 * (1 - high), 1000 * (1 - low)],
    change: function(event, ui) {
      low = 1 - ui.values[1] / 1000.0; high = 1 - ui.values[0] / 1000.0;
      colourize();
    }
  });
  $('#ui-gamma').slider({ orientation: 'vertical', min: 0, max: 1000, step: 1,
    value: 1000 * (Math.log(gamma)/5 + 0.5),
    change: function(event, ui) {
      gamma = Math.exp(5 * (ui.value / 1000.0 - 0.5));
      colourize();
    }
  });
  colourize();
}
